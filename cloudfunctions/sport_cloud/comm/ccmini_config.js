// +----------------------------------------------------------------------
// | CCMiniCloud [ Cloud Framework ]
// +----------------------------------------------------------------------
// | Copyright (c) 2021 www.code942.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 明章科技
// +----------------------------------------------------------------------

/**
 * Notes: 配置文件
 * Ver : CCMiniCloud Framework 2.0.1 ALL RIGHTS RESERVED BY www.code942.com
 * Date: 2021-08-02 19:20:00
 */
module.exports = {
	CCMINI_CLOUD_ID: 'card-8gdoyuexd3702a23', // 云服务id  

	CCMINI_ADMIN_NAME: 'ccadmin', // 管理员账号（6-30位)
	CCMINI_ADMIN_PWD: '123456', // 管理员密码（6-30位)

	CCMINI_TEST_MODE: false,

	CCMINI_TEST_TOKEN_ID: 'oTthr5P34HTx5iYLtATaGjNlIxZw',

	CCMINI_ADMIN_LOGIN_EXPIRE: 86400,

	PROJECT_MARK: 'sport',
	PROJECT_NAME: 'CC校园运动小程序',
	PROJECT_VER: 'CCSport-Cloud(V1.3 Build20210826)', //升级必须，请勿修改
	PROJECT_SOURCE: 'https://gitee.com/minzonetech',

	CCMINI_CLIENT_CHECK_CONTENT: true,
	CCMINI_ADMIN_CHECK_CONTENT: false,
 
	CCMINI_COLLECTION_NAME: 'setup|user|admin|news|meet|join',
	CCMINI_SETUP_TITLE: 'CC校园运动小程序',
	CCMINI_SETUP_ABOUT: '   校园运动小程序简介！！！',

	CCMINI_NEWS_TITLE: '校园运动小程序正式上线了！！',
	CCMINI_NEWS_CATE: '公告通知',
	CCMINI_NEWS_DESC: '经过大家的努力，我们的校园运动小程序正式上线了，希望大家多多光临，互勉！！',
	CCMINI_NEWS_CONTENT: '经过大家的努力，我们的校园运动小程序正式上线了，希望大家多多光临，互勉！！',

}